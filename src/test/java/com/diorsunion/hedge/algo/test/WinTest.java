package com.diorsunion.hedge.algo.test;

import com.diorsunion.hedge.algo.Operation;
import com.diorsunion.hedge.base.RealDataSourceBOBaseTest;
import com.diorsunion.hedge.domain.AccountManager;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import com.diorsunion.hedge.dal.entity.TradeLog;

import java.util.Date;
import java.util.List;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class WinTest extends RealDataSourceBOBaseTest {
    public static double dayoper(List<Date> dates, AccountManager accountManager, List<AccountManager> account_per_days, Operation operation){
        int day = 2;
        for (Date date : dates) {
            AccountManager nextAccount = accountManager.initNextDayAccount(date);
            account_per_days.add(nextAccount);
//            RandomWinTest.printStockDetail(day, date, nextAccount, "操作前");
            operation.oper(nextAccount, account_per_days);
//            RandomWinTest.printStockDetail(day, date, nextAccount, "操作后");
//            System.out.println();
            accountManager = nextAccount;
            day++;
        }
        List<TradeLog> tradeLogs = accountManager.close(StockPrice.PriceType.CLOSE);//最后一天，全部平仓
        operation.tradeLogs.addAll(tradeLogs);
        double lastValue = accountManager.getTotalValue(StockPrice.PriceType.CLOSE);
        return lastValue;
    }
}
