package com.diorsunion.hedge.dal.repository.stock.test;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.hedge.base.RepositoryBaseTest;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPriceEx;
import com.diorsunion.hedge.dal.repository.stock.StockPriceExRepository;
import com.diorsunion.hedge.domain.Trend;
import com.diorsunion.hedge.util.CalendarUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * @author harley-dog on 2015/6/4.
 */
public class StockPriceExRepositoryTest extends RepositoryBaseTest {

    @Resource
    StockPriceExRepository repository;

    Stock stock;
    StockPriceEx entity = new StockPriceEx();
    @Before
    public void before(){
        stock =  new Stock();
        stock.code = "BABA";
        entity.ema12 = 0.1;
        entity.ema26 = 0.2;
        entity.dea = 0.3;
        entity.dif = 0.4;
        entity.macd = 0.11;
        entity.macdReversePrice = 80;
        entity.macdTrend = Trend.U;
        entity.bollLow = 1;
        entity.bollMid = 2;
        entity.bollUp = 3;
        entity.k = 0.2;
        entity.d = 0.3;
        entity.j = 0.4;
        entity.rsi1 = 55;
        entity.rsi2 = 46;
        entity.rsi3 = 63;
        entity.stock = stock;
        entity.thedate = new Date();
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPriceEx.class, number = 1, tableName = "stock_price_day_ex",custom = "macd_trend=U")
    })
    public void testGet() {
        StockPriceEx entity = repository.get(1, Period.day);
        Assert.assertNotNull(entity);
        Assert.assertEquals(Trend.U,entity.macdTrend);
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPriceEx.class, number = 3, tableName = "stock_price_day_ex",custom = "id={1,2,3};stock_code=BABA;macd_trend={U,E,D}")
    })
    public void testFind() {
        StockPriceEx query = new StockPriceEx();
        query.stock = stock;
        List<StockPriceEx> list = repository.find(query, Period.day);
        assert (list != null);
        Assert.assertEquals(list.get(0).id,1);
        Assert.assertEquals(list.get(1).id,2);
        Assert.assertEquals(list.get(2).id,3);

        Assert.assertEquals(list.get(0).macdTrend,Trend.U);
        Assert.assertEquals(list.get(1).macdTrend,Trend.E);
        Assert.assertEquals(list.get(2).macdTrend,Trend.D);
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPriceEx.class, number = 3, tableName = "stock_price_day_ex",custom = "id={1,2,3};stock_code=BABA;macd_trend=U;thedate={2015-12-01,2015-12-03,2015-12-02}")
    })
    public void testGetMaxDate() throws ParseException {
        Date thedate = CalendarUtils.parseSimple("2016-01-01");
        Date maxDate = repository.getMaxDate(stock, thedate, Period.day);
        Date expect = CalendarUtils.parseSimple("2015-12-03");
        Assert.assertEquals(expect,maxDate);
        Date thedate1 = CalendarUtils.parseSimple("2014-01-01");
        maxDate = repository.getMaxDate(stock, thedate1, Period.day);
        Assert.assertNull(maxDate);
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPriceEx.class, number = 3, tableName = "stock_price_day_ex",custom = "id={1,2,3};stock_code=BABA;macd_trend={U,E,D};thedate={2015-12-01,2015-12-03,2015-12-02}")
    })
    public void testFindStockPriceEx() throws ParseException {
        Date begin = CalendarUtils.parseSimple("2015-12-01");
        Date end = CalendarUtils.parseSimple("2015-12-03");
        List<StockPriceEx> list = repository.findStockPriceEx(stock, begin, end, Period.day);
        Assert.assertEquals(list.get(0).id,1);
        Assert.assertEquals(list.get(1).id,3);
        Assert.assertEquals(list.get(2).id,2);
        Assert.assertEquals(list.get(0).stock.code,"BABA");
        Assert.assertEquals(list.get(1).stock.code,"BABA");
        Assert.assertEquals(list.get(2).stock.code,"BABA");
        Assert.assertEquals(list.get(0).macdTrend,Trend.U);
        Assert.assertEquals(list.get(1).macdTrend,Trend.D);
        Assert.assertEquals(list.get(2).macdTrend,Trend.E);
        Assert.assertEquals(list.get(0).thedate,CalendarUtils.parseSimple("2015-12-01"));
        Assert.assertEquals(list.get(1).thedate,CalendarUtils.parseSimple("2015-12-02"));
        Assert.assertEquals(list.get(2).thedate,CalendarUtils.parseSimple("2015-12-03"));
        assert (list.get(0).thedate.getTime() == CalendarUtils.parseSimple("2015-12-01").getTime());
        assert (list.get(1).thedate.getTime() == CalendarUtils.parseSimple("2015-12-02").getTime());
        assert (list.get(2).thedate.getTime() == CalendarUtils.parseSimple("2015-12-03").getTime());
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPriceEx.class, number = 0, tableName = "stock_price_day_ex"),
            @DataSet(entityClass = StockPriceEx.class, number = 0, tableName = "stock_price_week_ex"),
            @DataSet(entityClass = StockPriceEx.class, number = 0, tableName = "stock_price_month_ex")
    })
    public void testInsertDelete() {
        for (Period period : Period.values()) {
            StockPriceEx entity1 = repository.get(entity.id, period);
            Assert.assertEquals (entity1 , null);
            repository.insert(entity, period);
            StockPriceEx stockPriceEx = repository.get(entity.id, period);
            Assert.assertEquals (entity.id , stockPriceEx.id);
            Assert.assertEquals (entity.ema26 , stockPriceEx.ema26,0.0001);
            Assert.assertEquals (entity.ema12 , stockPriceEx.ema12,0.0001);
            Assert.assertEquals (entity.dea , stockPriceEx.dea,0.0001);
            Assert.assertEquals (entity.dif , stockPriceEx.dif,0.0001);
            Assert.assertEquals (entity.macd , stockPriceEx.macd,0.0001);
            Assert.assertEquals (entity.k , stockPriceEx.k,0.0001);
            Assert.assertEquals (entity.d , stockPriceEx.d,0.0001);
            Assert.assertEquals (entity.j , stockPriceEx.j,0.0001);
            Assert.assertEquals (entity.rsi1 , stockPriceEx.rsi1,0.0001);
            Assert.assertEquals (entity.rsi2 , stockPriceEx.rsi2,0.0001);
            Assert.assertEquals (entity.rsi3 , stockPriceEx.rsi3,0.0001);
            Assert.assertEquals (entity.bollLow , stockPriceEx.bollLow,0.0001);
            Assert.assertEquals (entity.bollUp , stockPriceEx.bollUp,0.0001);
            Assert.assertEquals (entity.bollMid , stockPriceEx.bollMid,0.0001);
            Assert.assertEquals (entity.macdReversePrice , stockPriceEx.macdReversePrice,0.0001);
            repository.delete(entity.id,period);
            StockPriceEx delete_Result = repository.get(entity.id, period);
            Assert.assertNull(delete_Result);
        }
    }

}
