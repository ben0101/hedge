package com.diorsunion.hedge.dal.repository.stock.test;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.hedge.base.RepositoryBaseTest;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import com.diorsunion.hedge.dal.entity.stock.StockPriceEx;
import com.diorsunion.hedge.dal.repository.stock.StockPriceExRepository;
import com.diorsunion.hedge.dal.repository.stock.StockPriceRepository;
import com.diorsunion.hedge.domain.Trend;
import com.diorsunion.hedge.util.CalendarUtils;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * @author harley-dog on 2015/6/4.
 */
public class StockPriceRepositoryTest extends RepositoryBaseTest {

    @Resource
    StockPriceRepository repository;
    @Resource
    StockPriceExRepository repositoryEx;


    StockPrice entity = new StockPrice();;
    Stock stock = new Stock();

    @Before
    public void before(){
        stock.code = "BABA";
        entity.chg = 0.1d;
        entity.close = 0.2d;
        entity.dea = 0.3d;
        entity.dif = 0.4d;
        entity.high = 0.5d;
        entity.low = 0.6d;
        entity.ma10 = 0.7d;
        entity.ma20 = 0.8d;
        entity.ma30 = 0.9d;
        entity.ma5 = 0.10d;
        entity.macd = 0.11d;
        entity.open = 0.32d;
        entity.percent = 0.13d;
        entity.volume = 10;
        entity.stock = stock;
        entity.turnrate = 0.26d;
        entity.thedate = new Date();
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_day")
    })
    public void testGet() {
        StockPrice entity = repository.get(1, Period.day);
        Assert.assertNotNull(entity);
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_day",custom = "id=1;stock_code=BABA;thedate=2016-01-01"),
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_week",custom = "id=1;stock_code=BABA;thedate=2016-01-01"),
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_month",custom = "id=1;stock_code=BABA;thedate=2016-01-01")
    })
    public void testFind() throws ParseException {
        StockPrice query = new StockPrice();
        query.stock = stock;
        query.thedate = CalendarUtils.parseSimple("2016-01-01");
        for (Period period : Period.values()) {
            List<StockPrice> stockPrices = repository.find(query, period);
            Assert.assertNotNull(stockPrices);
            Assert.assertEquals(stockPrices.get(0).id, 1);
            Assert.assertEquals(stockPrices.get(0).stock.code,"BABA");
            Assert.assertEquals(stockPrices.get(0).thedate,query.thedate);
        }
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 3, tableName = "stock_price_day",custom = "id={1,2,3};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03}"),
            @DataSet(entityClass = StockPrice.class, number = 3, tableName = "stock_price_week",custom = "id={1,2,3};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03}"),
            @DataSet(entityClass = StockPrice.class, number = 3, tableName = "stock_price_month",custom = "id={1,2,3};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03}"),
            @DataSet(entityClass = StockPriceEx.class, number = 2, tableName = "stock_price_day_ex",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02};k={11,22};macd_reverse_price={33,44};macd_trend={U,D}"),
            @DataSet(entityClass = StockPriceEx.class, number = 2, tableName = "stock_price_week_ex",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02};k={11,22};macd_reverse_price={33,44};macd_trend={U,D}"),
            @DataSet(entityClass = StockPriceEx.class, number = 2, tableName = "stock_price_month_ex",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02};k={11,22};macd_reverse_price={33,44};macd_trend={U,D}")
    })
    public void testFindWithEx() throws ParseException {
        StockPrice query = new StockPrice();
        query.stock = stock;
        query.thedate = CalendarUtils.parseSimple("2016-01-01");
        for (Period period : Period.values()) {
            List<StockPrice> stockPrices = repository.findWithEx(query, period);
            Assert.assertEquals (stockPrices.size(),1);
            Assert.assertEquals (stockPrices.get(0).id,1);
            Assert.assertEquals (stockPrices.get(0).stock.code,"BABA");
            Assert.assertEquals(stockPrices.get(0).thedate, CalendarUtils.parseSimple("2016-01-01"));
            Assert.assertEquals (stockPrices.get(0).ex.k,11d,0.0001);
            Assert.assertEquals (stockPrices.get(0).ex.macdReversePrice,33d,0.0001);
            Assert.assertEquals (stockPrices.get(0).ex.macdTrend, Trend.U);
        }
    }


    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 2, tableName = "stock_price_day",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02}"),
            @DataSet(entityClass = StockPrice.class, number = 2, tableName = "stock_price_week",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02}"),
            @DataSet(entityClass = StockPrice.class, number = 2, tableName = "stock_price_month",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02}")
    })
    public void testFindStockPrice() throws ParseException {
        Date begin = CalendarUtils.parseSimple("2016-01-01");
        Date end = CalendarUtils.parseSimple("2016-01-02");
        for (Period period : Period.values()) {
            List<StockPrice> stockPrices = repository.findStockPrice(stock, begin, end, period);
            Assert.assertEquals (stockPrices.size(),2);
            Assert.assertEquals (stockPrices.get(0).id,1);
            Assert.assertEquals (stockPrices.get(0).stock.code,"BABA");
            Assert.assertEquals (stockPrices.get(0).thedate,CalendarUtils.parseSimple("2016-01-01"));
            Assert.assertEquals (stockPrices.get(1).id,2);
            Assert.assertEquals (stockPrices.get(1).stock.code,"BABA");
            Assert.assertEquals (stockPrices.get(1).thedate,CalendarUtils.parseSimple("2016-01-02"));
        }
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 3, tableName = "stock_price_day",custom = "id={1,2,3};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03}"),
            @DataSet(entityClass = StockPrice.class, number = 3, tableName = "stock_price_week",custom = "id={1,2,3};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03}"),
            @DataSet(entityClass = StockPrice.class, number = 3, tableName = "stock_price_month",custom = "id={1,2,3};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03}"),
            @DataSet(entityClass = StockPriceEx.class, number = 2, tableName = "stock_price_day_ex",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02};k={11,22};macd_reverse_price={33,44};macd_trend={U,D}"),
            @DataSet(entityClass = StockPriceEx.class, number = 2, tableName = "stock_price_week_ex",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02};k={11,22};macd_reverse_price={33,44};macd_trend={U,D}"),
            @DataSet(entityClass = StockPriceEx.class, number = 2, tableName = "stock_price_month_ex",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02};k={11,22};macd_reverse_price={33,44};macd_trend={U,D}")
    })
    public void testFindStockPriceWithEx() throws ParseException {
        Date begin = CalendarUtils.parseSimple("2016-01-01");
        Date end = CalendarUtils.parseSimple("2016-01-03");
        for (Period period : Period.values()) {
            List<StockPrice> stockPrices = repository.findStockPriceWithEx(stock, begin, end, period);
            Assert.assertEquals (stockPrices.size(),3);
            Assert.assertEquals (stockPrices.get(0).id,1);
            Assert.assertEquals (stockPrices.get(0).stock.code,"BABA");
            Assert.assertEquals (stockPrices.get(0).thedate,CalendarUtils.parseSimple("2016-01-01"));
            Assert.assertEquals (stockPrices.get(0).ex.k,11d,0.0001);
            Assert.assertEquals (stockPrices.get(0).ex.macdReversePrice,33d,0.0001);
            Assert.assertEquals (stockPrices.get(0).ex.macdTrend, Trend.U);
            Assert.assertEquals (stockPrices.get(1).id,2);
            Assert.assertEquals (stockPrices.get(1).stock.code,"BABA");
            Assert.assertEquals (stockPrices.get(1).thedate,CalendarUtils.parseSimple("2016-01-02"));
            Assert.assertEquals (stockPrices.get(1).ex.k,22d,0.0001);
            Assert.assertEquals (stockPrices.get(1).ex.macdReversePrice,44d,0.0001);
            Assert.assertEquals (stockPrices.get(1).ex.macdTrend, Trend.D);
            Assert.assertEquals (stockPrices.get(1).id,2);
            Assert.assertEquals (stockPrices.get(1).stock.code,"BABA");
            Assert.assertNull (stockPrices.get(2).ex.thedate);
            Assert.assertEquals (stockPrices.get(2).ex.k,0,0.0001);
            Assert.assertEquals (stockPrices.get(2).ex.macdReversePrice,0,0.0001);
        }
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 5, tableName = "stock_price_day",custom = "id={1,2,3,4,5};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03,2016-01-04,2016-01-05}"),
            @DataSet(entityClass = StockPrice.class, number = 5, tableName = "stock_price_week",custom = "id={1,2,3,4,5};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03,2016-01-04,2016-01-05}"),
            @DataSet(entityClass = StockPrice.class, number = 5, tableName = "stock_price_month",custom = "id={1,2,3,4,5};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03,2016-01-04,2016-01-05}"),
            @DataSet(entityClass = StockPriceEx.class, number = 3, tableName = "stock_price_day_ex",custom = "id={1,2,3};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03};k={11,22,33};macd_trend={U,D,E}"),
            @DataSet(entityClass = StockPriceEx.class, number = 3, tableName = "stock_price_week_ex",custom = "id={1,2,3};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03};k={11,22,33};macd_trend={U,D,E}"),
            @DataSet(entityClass = StockPriceEx.class, number = 3, tableName = "stock_price_month_ex",custom = "id={1,2,3};stock_code=BABA;thedate={2016-01-01,2016-01-02,2016-01-03};k={11,22,33};macd_trend={U,D,E}")
    })
    public void testFindStockPriceWithExNum() throws ParseException {
        Date end = CalendarUtils.parseSimple("2016-01-06");
        for (Period period : Period.values()) {
            Date maxDate = repositoryEx.getMaxDate(stock,end,period);
            List<StockPrice> stockPrices = repository.findStockPriceWithExNum(stock, maxDate,  2,period);
            Assert.assertEquals (stockPrices.size(),2);
            Assert.assertEquals (stockPrices.get(0).id,2);
            Assert.assertEquals (stockPrices.get(0).stock.code,"BABA");
            Assert.assertEquals (stockPrices.get(0).thedate,CalendarUtils.parseSimple("2016-01-02"));
            Assert.assertEquals (stockPrices.get(0).ex.k,22d,0.0001);
            Assert.assertEquals (stockPrices.get(0).ex.macdTrend, Trend.D);
            Assert.assertEquals (stockPrices.get(0).ex.thedate, CalendarUtils.parseSimple("2016-01-02"));
            Assert.assertEquals (stockPrices.get(1).id,3);
            Assert.assertEquals (stockPrices.get(1).stock.code,"BABA");
            Assert.assertEquals (stockPrices.get(1).thedate,CalendarUtils.parseSimple("2016-01-03"));
            Assert.assertEquals (stockPrices.get(1).ex.k,33d,0.0001);
            Assert.assertEquals (stockPrices.get(1).ex.macdTrend, Trend.E);
            Assert.assertEquals (stockPrices.get(1).ex.thedate, CalendarUtils.parseSimple("2016-01-03"));
        }

        Stock stock = new Stock("BIDU");
        for (Period period : Period.values()) {
            List<StockPrice> stockPrices = repository.findStockPriceWithExNum(stock, CalendarUtils.parseSimple("2016-01-06"),2,period);
            Assert.assertEquals (stockPrices.size(),0);
        }
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 2, tableName = "stock_price_day",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02}"),
            @DataSet(entityClass = StockPrice.class, number = 2, tableName = "stock_price_week",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02}"),
            @DataSet(entityClass = StockPrice.class, number = 2, tableName = "stock_price_month",custom = "id={1,2};stock_code=BABA;thedate={2016-01-01,2016-01-02}")
    })
    public void testFindByIds() throws ParseException {
        for (Period period : Period.values()) {
            List<StockPrice> stockPrices = repository.findByIds(Lists.newArrayList(1,2),period);
            Assert.assertEquals (stockPrices.get(0).id,1);
            Assert.assertEquals (stockPrices.get(0).stock.code ,"BABA");
            Assert.assertEquals (stockPrices.get(0).thedate,CalendarUtils.parseSimple("2016-01-01"));
            Assert.assertEquals (stockPrices.get(1).id,2);
            Assert.assertEquals (stockPrices.get(1).stock.code,"BABA");
            Assert.assertEquals (stockPrices.get(1).thedate,CalendarUtils.parseSimple("2016-01-02"));
        }
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 0, tableName = "stock_price_day"),
            @DataSet(entityClass = StockPrice.class, number = 0, tableName = "stock_price_week"),
            @DataSet(entityClass = StockPrice.class, number = 0, tableName = "stock_price_month")
    })
    public void testInsertDelete() {
        for (Period period : Period.values()) {
            StockPrice nullStockPrice = repository.get(entity.id, period);
            Assert.assertEquals (nullStockPrice , null);
            repository.insert(entity, period);
            StockPrice stockPrice = repository.get(entity.id, period);
            Assert.assertEquals (entity.id , stockPrice.id);
            Assert.assertEquals (entity.chg , stockPrice.chg,0.0001);
            Assert.assertEquals (entity.open , stockPrice.open,0.0001);
            Assert.assertEquals (entity.close , stockPrice.close,0.0001);
            Assert.assertEquals (entity.dea , stockPrice.dea,0.0001);
            Assert.assertEquals (entity.dif , stockPrice.dif,0.0001);
            Assert.assertEquals (entity.high , stockPrice.high,0.0001);
            Assert.assertEquals (entity.low , stockPrice.low,0.0001);
            Assert.assertEquals (entity.macd , stockPrice.macd,0.0001);
            Assert.assertEquals (entity.ma5 , stockPrice.ma5,0.0001);
            Assert.assertEquals (entity.ma10 , stockPrice.ma10,0.0001);
            Assert.assertEquals (entity.ma20 , stockPrice.ma20,0.0001);
            Assert.assertEquals (entity.ma30 , stockPrice.ma30,0.0001);
            Assert.assertEquals (entity.percent , stockPrice.percent,0.0001);
            Assert.assertEquals (entity.volume , stockPrice.volume,0.0001);
            repository.delete(entity.id,period);
            nullStockPrice = repository.get(entity.id, period);
            Assert.assertNull(nullStockPrice);
        }
    }

}
