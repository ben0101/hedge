package com.diorsunion.hedge.ib.test;

import com.diorsunion.hedge.base.ExternalApiBaseTest;
import com.google.common.collect.Lists;
import com.ib.client.*;
import org.junit.Test;
import com.ib.api.samples.rfq.SimpleWrapper;

import java.util.List;
import java.util.Vector;

/**
 * Created by harley-dog on 2016/4/25.
 */
public class IBConnectTest extends ExternalApiBaseTest{

    @Test
    public void test() throws InterruptedException {
        int tickerId = 1;
        Contract contract = new Contract() ;
        contract.m_conId = 1;
        contract.m_symbol = "UVXY";
        contract.m_secType = "STK";
        contract.m_expiry = "";
        contract.m_strike = 0d;
        contract.m_right = "";
        contract.m_multiplier = "";
        contract.m_exchange = "";
        contract.m_currency = "USD";
        contract.m_localSymbol = "";
        contract.m_tradingClass = "";
        contract.m_primaryExch = "ISLAND";
        contract.m_includeExpired = false;
        contract.m_exchange = "SMART";
        contract.m_currency = "USD";
        contract.m_localSymbol = "";
        contract.m_secId = "";
        contract.m_secIdType = "";
        contract.m_comboLegs = new Vector<>();
        String genericTickList = "100,101,104,105,106,107,165,221,225,233,236,258,293,294,295,318";
        boolean snapshot = false;
        List<TagValue > mktDataOptions = Lists.newArrayList();
        EWrapper myWrapper = new MyWrapper();
        final EClientSocket m_s = new EClientSocket(myWrapper);
        m_s.eConnect("127.0.0.1", 7496, 2);
        if(m_s.isConnected()){
            System.out.println("Connected to Tws server version " +
                    m_s.serverVersion() + " at " +
                    m_s.TwsConnectionTime());
        }
        m_s.reqCurrentTime();
//        m_s.reqAccountSummary(1,"g1","1");
//        m_s.reqMktData(tickerId, contract, genericTickList, snapshot, mktDataOptions);
        m_s.reqPositions();
        m_s.reqAccountSummary(100, "All", "AccruedCash,BuyingPower,NetLiquidation");
//        m_s.reqHistoricalData(tickerId,contract);
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                m_s.reqMktData(tickerId,contract,genericTickList,snapshot,mktDataOptions);
//            }
//        });
//        thread.start();
        Thread.sleep(3000);
    }

    class MyWrapper extends SimpleWrapper {

    }
}
