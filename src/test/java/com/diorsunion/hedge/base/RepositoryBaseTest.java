package com.diorsunion.hedge.base;

import com.diorsunion.dbtest.spring.DBTestClassRunner;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

/**
 * @author harley-dog on 2015/6/4.
 */

@RunWith(DBTestClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:springbeans-ds-embedded-test.xml",
        "classpath:springbeans-mybatis.xml"
})
@Category(RepositoryBaseTest.class)
public class RepositoryBaseTest extends BaseTest{
    @Test
    public void test() {

    }

    @Rule
    public TestName name = new TestName();

    @Override
    protected String getBaseName() {
        return "持久层内存数据库单元测试";
    }

    @Override
    protected TestName getTestName() {
        return name;
    }
}
