package com.diorsunion.hedge.util.test;

import com.diorsunion.hedge.base.UnitBaseTest;
import com.diorsunion.hedge.util.CollectionTools;
import com.diorsunion.hedge.util.MathUtils;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;

import java.util.List;

/**
 * Created by harley-dog on 2016/3/29.
 */
public class MathUtilsTest extends UnitBaseTest {
    @Test
    public void test(){
        double a0 = 100;
        double a1 = 100;
        int index = 10;
        for(int i=0;i<10;i++){
            a0 = a0*1.1;
            a1 = a1*0.9;
            System.out.println("a0="+f(a0)+";a1="+f(a1)+";total="+f(a0+a1));
        }
        System.out.println("a0="+f(a0)+";a1="+f(a1)+";total="+f(a0+a1));
        for(int i=0;i<10;i++){
            a0 = a0*0.9;
            a1 = a1*1.1;
            System.out.println("a0="+f(a0)+";a1="+f(a1)+";total="+f(a0+a1));
        }
        System.out.println("a0="+f(a0)+";a1="+f(a1)+";total="+f(a0+a1));
    }

    @Test
    public void testShouYiLv(){
        double b = 100000;
        for(int i=1;i<=1000;i++){
            b *= 1.001d;
        }
        System.out.println(b);
    }

    @Test
    public void testEquivalent(){
        double a = 10.12345678;
        double b = 10.12345679;
        Assert.assertNotEquals(a, b);
        Assert.assertTrue(MathUtils.equivalent(a, b, 6));
        Assert.assertTrue(MathUtils.equivalent(a, b, 7));
        Assert.assertFalse(MathUtils.equivalent(a, b, 8));
        Assert.assertFalse(MathUtils.equivalent(a, b, 9));
    }

    @Test
    public void testGetMaxMin(){
        List<Double> list = Lists.newArrayList(1d,2.4d,3d,4.4d,5d,5.2d,7.5d,6.5d,4.112d,4.113d,0.923d,0.966d);
        double[] arr = CollectionTools.toDoubleArray(list);
        double[] maxmin = MathUtils.getMaxMin(arr);
        list.sort((a,b)-> new Double(a).compareTo(new Double(b)));
        Assert.assertEquals(maxmin[0], list.get(list.size() - 1), 0.0001);
        Assert.assertEquals(maxmin[1], list.get(0), 0.0001);
    }

    @Test
    public void testSum(){
        double[] a = {1.1,2.2,3.333,-4.44};
        double sum = MathUtils.sum(a);
        Assert.assertEquals(sum,a[0]+a[1]+a[2]+a[3],0.0001);
    }

    @Test
    public void testStd(){
        double[] a = {5,6,8,9};
        double std = MathUtils.std(a);
        Assert.assertEquals(std,1.5811,0.0001);
    }

    private String f(double d){
        return String.format("%.2f",d);
    }

}
