package com.diorsunion.hedge.util.test;

import com.diorsunion.hedge.base.UnitBaseTest;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

/**
 * Created by harley-dog<custer7572@163.com> on 2015/12/10.
 */
public class LambdaTest extends UnitBaseTest{
    public static void print(Object obj) {
        System.out.println(obj);
    }

    @Test
    public void test() {

        List<Object> objects = Lists.newArrayList();
        objects.parallelStream().forEach(LambdaTest::print);
    }
}
