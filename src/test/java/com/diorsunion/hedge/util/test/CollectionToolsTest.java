package com.diorsunion.hedge.util.test;

import com.diorsunion.hedge.util.CollectionTools;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by harley-dog on 2016/5/5.
 */
public class CollectionToolsTest {

    @Test
    public void testToDoubleArray(){
        List<Double> list = Lists.newArrayList(1d,2d);
        double[] a = CollectionTools.toDoubleArray(list);
        Assert.assertEquals(a[0],1,0.001);
        Assert.assertEquals(a[1],2,0.001);
    }
}
