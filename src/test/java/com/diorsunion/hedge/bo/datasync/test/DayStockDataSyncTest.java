package com.diorsunion.hedge.bo.datasync.test;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.hedge.base.EmbeddedBOBaseTest;
import com.diorsunion.hedge.bo.net.DataFetcher;
import com.diorsunion.hedge.bo.datasync.DayDataSync;
import com.diorsunion.hedge.bo.db.StockPriceBO;
import com.diorsunion.hedge.dal.entity.stock.StockPriceEx;
import com.diorsunion.hedge.dal.repository.stock.StockPriceExRepository;
import com.diorsunion.hedge.util.CalendarUtils;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.util.ReflectionUtil;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import com.diorsunion.hedge.dal.repository.stock.StockPriceRepository;
import com.diorsunion.hedge.dal.repository.stock.StockRepository;
import com.google.common.collect.Lists;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by harley-dog<custer7572@163.com> on 15/12/16.
 */
public class DayStockDataSyncTest extends EmbeddedBOBaseTest {
    @Resource
    protected StockPriceRepository stockPriceRepository;
    @Resource
    protected StockPriceExRepository stockPriceExRepository;
    @Resource
    protected StockRepository stockRepository;
    @Resource
    protected StockPriceBO stockPriceBO;
    DayDataSync stockDataSync = new DayDataSync();
    DataFetcher dataFetcher = EasyMock.createMock(DataFetcher.class);
    Stock stock = new Stock();
    Date today;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void before() throws NoSuchFieldException, IllegalAccessException, ParseException {
        ReflectionUtil.setValue(stockDataSync, "dataFetcher", dataFetcher);
        ReflectionUtil.setValue(stockDataSync, "stockPriceRepository", stockPriceRepository);
        ReflectionUtil.setValue(stockDataSync, "stockPriceExRepository", stockPriceExRepository);
        ReflectionUtil.setValue(stockDataSync, "stockRepository", stockRepository);
        ReflectionUtil.setValue(stockDataSync, "stockPriceBO", stockPriceBO);
        stock.code = "SQQQ";
        today = simpleDateFormat.parse("2015-12-28");
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_day", custom = "id=1;stock_code=SQQQ;thedate=2015-12-25")
    })
    public void testGetBegin1() throws ParseException {
        Date begin = stockDataSync.getBegin(simpleDateFormat.parse("2015-12-28"), stock);
        Date expect = simpleDateFormat.parse("2015-12-26");
        Assert.assertEquals(begin,expect);
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_day", custom = "id=1;stock_code=SQQQ;thedate=2015-12-28")
    })
    public void testGetBegin2() throws ParseException {
        Date begin = stockDataSync.getBegin(simpleDateFormat.parse("2015-12-30"), stock);
        Date expect = simpleDateFormat.parse("2015-12-29");
        Assert.assertEquals(begin,expect);
    }

    @Test
    public void testGetEnd() throws ParseException {
        Date end = stockDataSync.getEnd(today, stock);
        Date expect = simpleDateFormat.parse("2015-12-28");
        Assert.assertEquals(end,expect);
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_day", custom = "id=1;stock_code=SQQQ;thedate=2015-11-30"),
            @DataSet(entityClass = StockPriceEx.class, number = 0, tableName = "stock_price_day_ex")
    })
    public void testSync() throws Exception {
        Date begin = simpleDateFormat.parse("2015-12-01");
        Date end = CalendarUtils.addDate(today, 0);
        List<StockPrice> list = getStockPrice();
        EasyMock.expect(dataFetcher.fetchStockPrice(stock, begin, end, Period.day)).andReturn(list);
        EasyMock.replay(dataFetcher);
        stockDataSync.sync(today, stock);

        List<StockPrice> stockPrices = stockPriceRepository.findStockPrice(stock, simpleDateFormat.parse("2015-11-30"), end, Period.day);
        assert (stockPrices.size() == 11);
        for (int i = 0; i < stockPrices.size(); i++) {
            StockPrice x = stockPrices.get(i);
            Assert.assertNotNull(x);
            Assert.assertNotNull(x.thedate);
            if (i > 0) {
                Assert.assertNotNull (x.preDate == stockPrices.get(i - 1).thedate);
            }
            if (i < stockPrices.size() - 1) {
                Assert.assertNotNull (x.nextDate== stockPrices.get(i + 1).thedate);
            }
        }
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 5, tableName = "stock_price_day",custom = "id={1,2,3,4,5};stock_code=SQQQ;thedate={2016-01-01,2016-01-02,2016-01-03,2016-01-04,2016-01-05}"),
            @DataSet(entityClass = StockPrice.class, number = 5, tableName = "stock_price_week",custom = "id={1,2,3,4,5};stock_code=SQQQ;thedate={2016-01-01,2016-01-02,2016-01-03,2016-01-04,2016-01-05}"),
            @DataSet(entityClass = StockPrice.class, number = 5, tableName = "stock_price_month",custom = "id={1,2,3,4,5};stock_code=SQQQ;thedate={2016-01-01,2016-01-02,2016-01-03,2016-01-04,2016-01-05}"),
            @DataSet(entityClass = StockPriceEx.class, number = 3, tableName = "stock_price_day_ex",custom = "id={1,2,3};stock_code=SQQQ;thedate={2016-01-01,2016-01-02,2016-01-03};k={11,22,33};macd_trend={U,D,E}"),
            @DataSet(entityClass = StockPriceEx.class, number = 3, tableName = "stock_price_week_ex",custom = "id={1,2,3};stock_code=SQQQ;thedate={2016-01-01,2016-01-02,2016-01-03};k={11,22,33};macd_trend={U,D,E}"),
            @DataSet(entityClass = StockPriceEx.class, number = 3, tableName = "stock_price_month_ex",custom = "id={1,2,3};stock_code=SQQQ;thedate={2016-01-01,2016-01-02,2016-01-03};k={11,22,33};macd_trend={U,D,E}")
    })
    public void testSyncEx() throws Exception {
        StockPrice query = new StockPrice();
        query.stock = stock;
        List<StockPrice> all = stockPriceRepository.find(query,Period.day);
        Assert.assertEquals(5, all.size());
        Date end = simpleDateFormat.parse("2016-01-06");
        stockDataSync.syncEx(stock, end);
        List<StockPrice> stockPrices = stockPriceRepository.findStockPriceWithEx(stock, simpleDateFormat.parse("2015-11-30"), end, Period.day);
        assert (stockPrices.size() == 5);
        for (int i = 0; i < stockPrices.size(); i++) {
            StockPrice x = stockPrices.get(i);
            Assert.assertNotNull(x);
            Assert.assertNotNull(x.thedate);
            Assert.assertEquals(x.thedate, x.ex.thedate);
            Assert.assertEquals(x.id,x.ex.id);
        }
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 5, tableName = "stock_price_day",custom = "id={1,2,3,4,5};stock_code=SQQQ;thedate={2016-01-01,2016-01-02,2016-01-03,2016-01-04,2016-01-05}"),
            @DataSet(entityClass = StockPrice.class, number = 5, tableName = "stock_price_week",custom = "id={1,2,3,4,5};stock_code=SQQQ;thedate={2016-01-01,2016-01-02,2016-01-03,2016-01-04,2016-01-05}"),
            @DataSet(entityClass = StockPrice.class, number = 5, tableName = "stock_price_month",custom = "id={1,2,3,4,5};stock_code=SQQQ;thedate={2016-01-01,2016-01-02,2016-01-03,2016-01-04,2016-01-05}"),
            @DataSet(entityClass = StockPriceEx.class, number = 0, tableName = "stock_price_day_ex"),
            @DataSet(entityClass = StockPriceEx.class, number = 0, tableName = "stock_price_week_ex"),
            @DataSet(entityClass = StockPriceEx.class, number = 0, tableName = "stock_price_month_ex")
    })
    public void testSyncEx2() throws Exception {
        StockPrice query = new StockPrice();
        query.stock = stock;
        List<StockPrice> all = stockPriceRepository.find(query,Period.day);
        Assert.assertEquals(5,all.size());
        Date end = simpleDateFormat.parse("2016-01-06");
        stockDataSync.syncEx(stock, end);
        List<StockPrice> stockPrices = stockPriceRepository.findStockPriceWithEx(stock, simpleDateFormat.parse("2015-11-30"), end, Period.day);
        assert (stockPrices.size() == 5);
        for (int i = 0; i < stockPrices.size(); i++) {
            StockPrice x = stockPrices.get(i);
            Assert.assertNotNull(x);
            Assert.assertNotNull(x.thedate);
            Assert.assertEquals(x.thedate, x.ex.thedate);
            Assert.assertEquals(x.id,x.ex.id);
        }
    }


    public List<StockPrice> getStockPrice() throws ParseException {
        StockPrice s1 = new StockPrice();
        StockPrice s2 = new StockPrice();
        StockPrice s3 = new StockPrice();
        StockPrice s4 = new StockPrice();
        StockPrice s5 = new StockPrice();
        StockPrice s6 = new StockPrice();
        StockPrice s7 = new StockPrice();
        StockPrice s8 = new StockPrice();
        StockPrice s9 = new StockPrice();
        StockPrice s0 = new StockPrice();
        s1.thedate = simpleDateFormat.parse("2015-12-01");
        s2.thedate = simpleDateFormat.parse("2015-12-02");
        s3.thedate = simpleDateFormat.parse("2015-12-03");
        s4.thedate = simpleDateFormat.parse("2015-12-04");
        s5.thedate = simpleDateFormat.parse("2015-12-05");
        s6.thedate = simpleDateFormat.parse("2015-12-08");
        s7.thedate = simpleDateFormat.parse("2015-12-09");
        s8.thedate = simpleDateFormat.parse("2015-12-10");
        s9.thedate = simpleDateFormat.parse("2015-12-11");
        s0.thedate = simpleDateFormat.parse("2015-12-12");
        return Lists.newArrayList(s1, s2, s3, s4, s5, s6, s7, s8, s9, s0);
    }
}
