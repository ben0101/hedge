package com.diorsunion.hedge.bo.quota.test;

import com.diorsunion.hedge.base.UnitBaseTest;
import com.diorsunion.hedge.quota.MACDFilter;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import com.diorsunion.hedge.util.CalendarUtils;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class MACDFilterTest extends UnitBaseTest {

    MACDFilter filter = new MACDFilter(12,26);

    Stock stock;
    StockPrice last = new StockPrice();

    List<StockPrice> lastList ;
    List<StockPrice> stockPriceList;
    @Before
    public void before() throws ParseException {
        stock = new Stock();
        stock.code = "BABA";
        last.stock = stock;
        last.thedate = CalendarUtils.parseSimple("2015-01-01");
        last.close = 100d;
        last.ex.macdReversePrice = 100d;
        last.ex.ema12 = 12d;
        last.ex.ema26 = 26d;
        last.ex.dea = 1d;
        last.ex.dif = 1.5d;
        last.ex.macd = 5;
        lastList = Lists.newArrayList(last);
        StockPrice s0 = new StockPrice();
        s0.stock = stock;
        s0.thedate = CalendarUtils.parseSimple("2015-01-02");
        s0.close = 110;
        StockPrice s1 = new StockPrice();
        s1.stock = stock;
        s1.thedate = CalendarUtils.parseSimple("2015-01-03");
        s1.close = 122;
        StockPrice s2 = new StockPrice();
        s2.stock = stock;
        s2.thedate = CalendarUtils.parseSimple("2015-01-04");
        s2.close = 105;
        stockPriceList = Lists.newArrayList(s0,s1,s2);
    }

    @Test
    public void testCustomMacdWithLast() throws ParseException {
        filter.filter(stock, lastList, stockPriceList);
        //第一次测试,测试lastList数据完整的情况
        StockPrice s0 = stockPriceList.get(0);
        double s0_ema12 = MACDFilter.calcEma12(last.ex.ema12, s0.close);//25.53846153846154
        double s0_ema26 = MACDFilter.calcEma26(last.ex.ema26, s0.close);
        double s0_dif = MACDFilter.calcDif(last.ex.ema12, last.ex.ema26, s0.close);
        double s0_dea = MACDFilter.calcDea(last.ex.ema12, last.ex.ema26, last.ex.dea, s0.close);
        double s0_macd = MACDFilter.calcMacdByPrice(last.ex.ema12, last.ex.ema26, last.ex.dea, s0.close);
        double s0_mace_reserve_price = MACDFilter.calcPriceByMacd(s0_ema12, s0_ema26, s0_dea, s0_macd);

        assertEquals(s0_ema12 ,s0.ex.ema12,0.0001);
        assertEquals (s0_ema26,s0.ex.ema26,0.0001);
        assertEquals (s0_dif,s0.ex.dif,0.0001);
        assertEquals (s0_dea,s0.ex.dea,0.0001);
        assertEquals (s0_macd,s0.ex.macd,0.0001);
        assertEquals (s0_mace_reserve_price,s0.ex.macdReversePrice,0.0001);

        StockPrice s1 = stockPriceList.get(1);
        double s1_ema12 = MACDFilter.calcEma12(s0_ema12, s1.close);//25.53846153846154
        double s1_ema26 = MACDFilter.calcEma26(s0_ema26, s1.close);
        double s1_dif = MACDFilter.calcDif(s0_ema12, s0_ema26, s1.close);
        double s1_dea = MACDFilter.calcDea(s0_ema12, s0_ema26,s0_dea, s1.close);
        double s1_macd = MACDFilter.calcMacdByPrice(s0_ema12, s0_ema26, s0_dea, s1.close);
        double s1_mace_reserve_price = MACDFilter.calcPriceByMacd(s1_ema12, s1_ema26, s1_dea, s1_macd);

        assertEquals (s1_ema12,s1.ex.ema12,0.0001);
        assertEquals (s1_ema26,s1.ex.ema26,0.0001);
        assertEquals (s1_dif,s1.ex.dif,0.0001);
        assertEquals (s1_dea,s1.ex.dea,0.0001);
        assertEquals (s1_macd,s1.ex.macd,0.0001);
        assertEquals (s1_mace_reserve_price,s1.ex.macdReversePrice,0.0001);


        StockPrice s2 = stockPriceList.get(2);
        double s2_ema12 = MACDFilter.calcEma12(s1_ema12, s2.close);//25.53846153846154
        double s2_ema26 = MACDFilter.calcEma26(s1_ema26, s2.close);
        double s2_dif = MACDFilter.calcDif(s1_ema12, s1_ema26, s2.close);
        double s2_dea = MACDFilter.calcDea(s1_ema12, s1_ema26,s1_dea, s2.close);
        double s2_macd = MACDFilter.calcMacdByPrice(s1_ema12, s1_ema26, s1_dea, s2.close);
        double s2_mace_reserve_price = MACDFilter.calcPriceByMacd(s2_ema12, s2_ema26, s2_dea, s2_macd);

        assertEquals (s2_ema12,s2.ex.ema12,0.0001);
        assertEquals (s2_ema26,s2.ex.ema26,0.0001);
        assertEquals (s2_dif,s2.ex.dif,0.0001);
        assertEquals (s2_dea,s2.ex.dea,0.0001);
        assertEquals (s2_macd,s2.ex.macd,0.0001);
        assertEquals (s2_mace_reserve_price,s2.ex.macdReversePrice,0.0001);
    }

    @Test
    public void testCustomMacdWithoutLast() throws ParseException {
        filter.filter(stock, null, stockPriceList);
        StockPrice s0 = stockPriceList.get(0);
        double s0_ema12 = s0.close;
        double s0_ema26 = s0.close;
        double s0_dif = MACDFilter.calcDif(s0_ema12, s0_ema26, s0.close);
        double s0_dea = MACDFilter.calcDea(s0_ema12, s0_ema26, s0_dif, s0.close);
        double s0_macd = MACDFilter.calcMacdByPrice(s0_ema12, s0_ema26, s0_dea, s0.close);
        double s0_mace_reserve_price = MACDFilter.calcPriceByMacd(s0_ema12, s0_ema26, s0_dea, s0_macd);

        assertEquals (s0_ema12,s0.ex.ema12,0.0001);
        assertEquals (s0_ema26,s0.ex.ema26,0.0001);
        assertEquals (s0_dif,s0.ex.dif,0.0001);
        assertEquals (s0_dea,s0.ex.dea,0.0001);
        assertEquals (s0_macd,s0.ex.macd,0.0001);
        assertEquals (s0_mace_reserve_price,s0.ex.macdReversePrice,0.0001);

        StockPrice s1 = stockPriceList.get(1);
        double s1_ema12 = MACDFilter.calcEma12(s0_ema12, s1.close);//25.53846153846154
        double s1_ema26 = MACDFilter.calcEma26(s0_ema26, s1.close);
        double s1_dif = MACDFilter.calcDif(s0_ema12, s0_ema26, s1.close);
        double s1_dea = MACDFilter.calcDea(s0_ema12, s0_ema26,s0_dea, s1.close);
        double s1_macd = MACDFilter.calcMacdByPrice(s0_ema12, s0_ema26, s0_dea, s1.close);
        double s1_mace_reserve_price = MACDFilter.calcPriceByMacd(s1_ema12, s1_ema26, s1_dea, s1_macd);


        assertEquals (s1_ema12,s1.ex.ema12,0.0001);
        assertEquals (s1_ema26,s1.ex.ema26,0.0001);
        assertEquals (s1_dif,s1.ex.dif,0.0001);
        assertEquals (s1_dea,s1.ex.dea,0.0001);
        assertEquals (s1_macd,s1.ex.macd,0.0001);
        assertEquals (s1_mace_reserve_price,s1.ex.macdReversePrice,0.0001);


        StockPrice s2 = stockPriceList.get(2);
        double s2_ema12 = MACDFilter.calcEma12(s1_ema12, s2.close);//25.53846153846154
        double s2_ema26 = MACDFilter.calcEma26(s1_ema26, s2.close);
        double s2_dif = MACDFilter.calcDif(s1_ema12, s1_ema26, s2.close);
        double s2_dea = MACDFilter.calcDea(s1_ema12, s1_ema26,s1_dea, s2.close);
        double s2_macd = MACDFilter.calcMacdByPrice(s1_ema12, s1_ema26, s1_dea, s2.close);
        double s2_mace_reserve_price = MACDFilter.calcPriceByMacd(s2_ema12, s2_ema26, s2_dea, s2_macd);

        assertEquals (s2_ema12,s2.ex.ema12,0.0001);
        assertEquals (s2_ema26,s2.ex.ema26,0.0001);
        assertEquals (s2_dif,s2.ex.dif,0.0001);
        assertEquals (s2_dea,s2.ex.dea,0.0001);
        assertEquals (s2_macd, s2.ex.macd,0.0001);
        assertEquals (s2_mace_reserve_price,s2.ex.macdReversePrice,0.0001);
    }



    @Test
    public void testCalcDifDeaMacdPriceByPrice(){
        double s_ema12 = 1d;
        double s_ema26 = 1.5d;
        double s_dea = 2d;
        double close = 10;

        double dif = MACDFilter.calcDif(s_ema12, s_ema26, close);
//        System.out.println("dif="+dif);
        double dea = MACDFilter.calcDea(s_ema12, s_ema26, s_dea, close);
//        System.out.println("dea="+dea);
        double macd = MACDFilter.calcMacdByPrice(s_ema12, s_ema26, s_dea, close);
//        System.out.println("macd="+macd);
        double price = MACDFilter.calcPriceByMacd(s_ema12, s_ema26, s_dea, macd);
//        System.out.println("price="+price);
        assertEquals(close,price,0.0001);
//        assertEquals (new BigDecimal(close).setScale(2,BigDecimal.ROUND_HALF_UP).equals(new BigDecimal(price).setScale(2,BigDecimal.ROUND_UP)));
    }
}
