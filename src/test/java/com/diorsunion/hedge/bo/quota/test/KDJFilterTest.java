package com.diorsunion.hedge.bo.quota.test;

import com.diorsunion.hedge.base.UnitBaseTest;
import com.diorsunion.hedge.quota.KDJFilter;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import com.diorsunion.hedge.util.CalendarUtils;
import com.diorsunion.hedge.util.CollectionTools;
import com.diorsunion.hedge.util.MathUtils;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class KDJFilterTest extends UnitBaseTest {

    KDJFilter filter = new KDJFilter(9,3,3);

    Stock stock;

    List<StockPrice> lastList;
    List<StockPrice> stockPriceList;

    double[] priceList = {110, 112, 114, 115, 113, 111, 109, 107, 108};
    double[] lastpriceList = {98, 102, 103};

    @Before
    public void before() throws ParseException {
        stock = new Stock();
        stock.code = "BABA";
        lastList = Lists.newArrayList();
        stockPriceList = Lists.newArrayList();
        for (int i = 0; i < lastpriceList.length; i++) {
            StockPrice s = new StockPrice();
            s.stock = stock;
            s.close = lastpriceList[i];
            s.thedate = CalendarUtils.parseSimple("2014-12-2" + (i + 7));
            double k = 50;
            double d = 50;
            if(i>0){
                StockPrice yes = lastList.get(lastList.size()-1);
                List<Double> doubles = Lists.newArrayList();
                for(int jj=0;jj<=i;jj++){
                    doubles.add(lastpriceList[jj]);
                }
                double[] a = CollectionTools.toDoubleArray(doubles);
                double[] maxmin = MathUtils.getMaxMin(a);
                double rsv = (lastpriceList[i]-maxmin[1])/(maxmin[0]-maxmin[1])*100;
                k = yes.ex.k*2/3 + rsv/3;
                d = yes.ex.d*2/3 + k/3;
            }
            s.ex.k = k;
            s.ex.d = d;
            s.ex.j = s.ex.k * 3 - s.ex.k * 2;
            lastList.add(s);
        }
        for (int i = 0; i < priceList.length; i++) {
            StockPrice s = new StockPrice();
            s.stock = stock;
            s.close = priceList[i];
            s.thedate = CalendarUtils.parseSimple("2015-01-0" + (i + 1));
            stockPriceList.add(s);
        }
    }

    @Test
    public void testKDJ() throws ParseException {
        filter.filter(stock, lastList, stockPriceList);
        for(int i=0;i<stockPriceList.size();i++){
            StockPrice stockPrice = stockPriceList.get(i);
            List<Double> doubles = Lists.newArrayList();
            for(int jj=0;jj<=i;jj++){
                doubles.add(stockPriceList.get(jj).close);
            }
            int c = 9-doubles.size();
            for(int jj=0,hh=lastpriceList.length-1 ; jj<c && hh>=0;jj++,hh--){
                doubles.add(lastpriceList[hh]);
            }
            double[] a = CollectionTools.toDoubleArray(doubles);
            double[] maxmin = MathUtils.getMaxMin(a);
            double rsv,e_k,e_d;
            if(i==0){
                rsv = (stockPrice.close-maxmin[1])/(maxmin[0]-maxmin[1])*100;
                e_k = lastList.get(2).ex.k*2/3 + rsv/3;
                e_d = lastList.get(2).ex.d*2/3 + e_k/3;
            }else {
                StockPrice stockPrice_yes = stockPriceList.get(i-1);
                rsv = (stockPrice.close-maxmin[1])/(maxmin[0]-maxmin[1])*100;
                e_k = stockPrice_yes.ex.k*2/3 + rsv/3;
                e_d = stockPrice_yes.ex.d*2/3 + e_k/3;
            }
            double e_j = e_k*3 - e_d*2;
            assertEquals (stockPrice.ex.k , e_k,0.0001);
            assertEquals (stockPrice.ex.d , e_d,0.0001);
            assertEquals (stockPrice.ex.j , e_j,0.0001);
        }
    }

}
