package com.diorsunion.hedge.bo.quota.test;

import com.diorsunion.hedge.base.RealDataSourceBOBaseTest;
import com.diorsunion.hedge.quota.MACDFilter;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import com.diorsunion.hedge.dal.repository.stock.StockPriceRepository;
import com.diorsunion.hedge.util.CalendarUtils;
import org.junit.Test;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static com.diorsunion.hedge.util.CurrencyUtils.*;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class MACDFilterRealTest extends RealDataSourceBOBaseTest {
    @Resource
    StockPriceRepository stockPriceRepository;
    MACDFilter macdFilter = new MACDFilter(12,26);

    @Test
    public void mutliStockMacd() throws ParseException {
        Stock stock = new Stock();
        stock.code = "UVXY";
        Date begin = CalendarUtils.parseSimple("2015-01-20");
        Date end = CalendarUtils.parseSimple("2016-04-30");
        List<StockPrice> stockPriceList = stockPriceRepository.findStockPrice(stock, begin, end, Period.day);
        macdFilter.filter(stock, null, stockPriceList);
        stockPriceList.stream()
                .filter(x -> !x.macds.isEmpty())
                .forEach(x -> System.out.println(stock.code + ":" + CalendarUtils.formatSimple(x.thedate) + ":" + x.macds));
    }

    @Test
    public void testCustomMacd() throws ParseException {
        Stock stock = new Stock();
        stock.code = "UVXY";
        Date begin = CalendarUtils.parseSimple("2010-01-20");
        Date end = CalendarUtils.parseSimple("2016-04-30");
        List<StockPrice> stockPriceList = stockPriceRepository.findStockPrice(stock, begin, end, Period.day);
        macdFilter.filter(stock, null, stockPriceList);
        stockPriceList.stream()
                .forEach(x -> {
                    System.out.println(stock.code + ":" + CalendarUtils.formatSimple(x.thedate) +
                            ",dif:" + format(x.dif) +
                            ",dea:" + format(x.dea) +
                            ",macd:" + format(x.macd) +
                            ",closePrice:" + format(x.close));
                    System.out.println(stock.code + ":" + CalendarUtils.formatSimple(x.thedate) +
                            ",dif:" + format(x.ex.dif) +
                            ",dea:" + format(x.ex.dea) +
                            ",macd:" + format(x.ex.macd) +
                            ",macdReversePrice:" + format(x.ex.macdReversePrice) +
                            ",macdtrend:" + x.ex.macdTrend);
                    System.out.println();
                });
    }

}
