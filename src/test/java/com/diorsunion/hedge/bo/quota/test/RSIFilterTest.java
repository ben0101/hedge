package com.diorsunion.hedge.bo.quota.test;

import com.diorsunion.hedge.base.UnitBaseTest;
import com.diorsunion.hedge.quota.RSIFilter;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import com.diorsunion.hedge.util.CalendarUtils;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.List;
import static org.junit.Assert.assertEquals;
/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class RSIFilterTest extends UnitBaseTest {

    RSIFilter filter = new RSIFilter(5,10,20);
    Stock stock;
    List<StockPrice> lastList;
    List<StockPrice> stockPriceList;

    double[] priceList = {110, 112, 114, 115, 113, 111, 109, 107, 108};
    double[] lastpriceList = {98, 102, 103};

    @Before
    public void before() throws ParseException {
        stock = new Stock();
        stock.code = "BABA";
        lastList = Lists.newArrayList();
        stockPriceList = Lists.newArrayList();
        for (int i = 0; i < lastpriceList.length; i++) {
            StockPrice s = new StockPrice();
            s.stock = stock;
            s.close = lastpriceList[i];
            s.thedate = CalendarUtils.parseSimple("2014-12-2" + (i + 7));
            if(i>0){
                s.chg = lastpriceList[i] - lastpriceList[i-1];
            }
            lastList.add(s);
        }
        for (int i = 0; i < priceList.length; i++) {
            StockPrice s = new StockPrice();
            s.stock = stock;
            s.close = priceList[i];
            s.thedate = CalendarUtils.parseSimple("2015-01-0" + (i + 1));
            if(i==0){
                s.chg = s.close - lastpriceList[lastpriceList.length-1];
            }else{
                s.chg = priceList[i] - priceList[i-1];
            }
            stockPriceList.add(s);
        }
    }

    @Test
    public void testRSI() throws ParseException {
        filter.filter(stock, lastList, stockPriceList);

        for(int i=0;i<stockPriceList.size();i++){
            StockPrice stockPrice = stockPriceList.get(i);
            int[] rsi_num = new int[filter.n.length];
            double[] a = new double[filter.n.length];
            double[] b = new double[filter.n.length];
            for(int k=0;k<filter.n.length;k++){
                for(int j=i;j>=0 && rsi_num[k]<filter.n[k];j--,rsi_num[k]++){
                    StockPrice s = stockPriceList.get(j);
                    if(s.chg>0){
                        a[k]+=s.chg;
                    }else if (s.chg<0){
                        b[k]-=s.chg;
                    }
                }
                for(int j=lastList.size()-1;j>=0 && rsi_num[k]<filter.n[k];j--,rsi_num[k]++){
                    StockPrice s = lastList.get(j);
                    if(s.chg>0){
                        a[k]+=s.chg;
                    }else if (s.chg<0){
                        b[k]-=s.chg;
                    }
                }
            }

            double[] rsis = new double[filter.n.length];
            for(int m=0 ;m< rsis.length;m++){
                double rs = b[m]==0?Double.MAX_VALUE:a[m]/b[m];
                rsis[m] = 100d-100d/(1+rs);
            }

            assertEquals(stockPrice.ex.rsi1,rsis[0],0.0001);
            assertEquals(stockPrice.ex.rsi2 ,rsis[1],0.0001);
            assertEquals(stockPrice.ex.rsi3 , rsis[2],0.0001);
        }
    }

}
