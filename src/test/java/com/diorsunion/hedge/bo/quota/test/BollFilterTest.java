package com.diorsunion.hedge.bo.quota.test;

import com.diorsunion.hedge.base.UnitBaseTest;
import com.diorsunion.hedge.quota.BollFilter;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import com.diorsunion.hedge.util.CalendarUtils;
import com.diorsunion.hedge.util.CollectionTools;
import com.diorsunion.hedge.util.MathUtils;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class BollFilterTest extends UnitBaseTest {

    BollFilter filter = new BollFilter(20, 2);
    Stock stock;
    List<StockPrice> lastList;
    List<StockPrice> stockPriceList;

    double[] priceList = {110, 112, 114, 115, 113, 111, 109, 107, 108};
    double[] lastpriceList = {98, 102, 103};

    @Before
    public void before() throws ParseException {
        stock = new Stock();
        stock.code = "BABA";
        lastList = Lists.newArrayList();
        stockPriceList = Lists.newArrayList();
        for (int i = 0; i < lastpriceList.length; i++) {
            StockPrice s = new StockPrice();
            s.stock = stock;
            s.close = lastpriceList[i];
            s.thedate = CalendarUtils.parseSimple("2014-12-2" + (i + 7));
            if (i > 0) {
                s.chg = lastpriceList[i] - lastpriceList[i - 1];
            }
            lastList.add(s);
        }
        for (int i = 0; i < priceList.length; i++) {
            StockPrice s = new StockPrice();
            s.stock = stock;
            s.close = priceList[i];
            s.thedate = CalendarUtils.parseSimple("2015-01-0" + (i + 1));
            if (i == priceList.length - 1) {
                s.ex.bollUp = 0;
                s.ex.bollMid = 0;
                s.ex.bollLow = 0;
            }
            stockPriceList.add(s);
        }
    }

    @Test
    public void testRSI() throws ParseException {
        filter.filter(stock, lastList, stockPriceList);

        for (int i = 0; i < stockPriceList.size(); i++) {
            StockPrice stockPrice = stockPriceList.get(i);
            List<Double> closes = Lists.newArrayList();//N-1日的close
            int d = 0;
            for (int j = i; j >= 0 && d < filter.n + 1; j--, d++) {
                closes.add(priceList[j]);
            }
            for (int j = lastpriceList.length - 1; j >= 0 && d < filter.n + 1; j--, d++) {
                closes.add(lastpriceList[j]);
            }
            //closes里存放了n+1天的数据
            double[] a = CollectionTools.toDoubleArray(closes);
            double[] a0 = Arrays.copyOfRange(a, 0, a.length - 1);
            double[] a1 = Arrays.copyOfRange(a, 1, a.length);
            double ma0 = MathUtils.avg(a0);
            double md = MathUtils.std(a1);
            double mb = ma0;
            double up = mb + filter.k * md;
            double dn = mb - filter.k * md;

            assertEquals(stockPrice.ex.bollLow, dn, 0.0001);
            assertEquals(stockPrice.ex.bollMid, mb, 0.0001);
            assertEquals(stockPrice.ex.bollUp, up, 0.0001);
        }
    }

}
