package com.diorsunion.hedge.bo.net.test;

import com.diorsunion.hedge.base.NetBaseTest;
import com.diorsunion.hedge.bo.net.XueQiuDataFetcher;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.util.ReflectionUtil;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by harley-dog<custer7572@163.com> on 15/12/15.
 */
public class XueQiuDataFetcherTest  extends NetBaseTest {


    XueQiuDataFetcher xueQiuDataFetcher = new XueQiuDataFetcher();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void before() throws IllegalAccessException {
        ReflectionUtil.setValue(xueQiuDataFetcher, "xueqiu_stock_price_url", "http://xueqiu.com/stock/forchartk/stocklist.json");
    }

    @Test
    public void test() throws Exception {
        Stock stock = new Stock();
        stock.code = "BABA";
        Date begin = simpleDateFormat.parse("2015-10-01");
        Date end = simpleDateFormat.parse("2015-12-12");
        List<StockPrice> stockPriceList_day = xueQiuDataFetcher.fetchStockPrice(stock, begin, end, Period.day);
        Assert.assertEquals(stockPriceList_day.size(),51);
        List<StockPrice> stockPriceList_week = xueQiuDataFetcher.fetchStockPrice(stock, begin, end, Period.week);
        Assert.assertEquals(stockPriceList_week.size(),11);
        List<StockPrice> stockPriceList_month = xueQiuDataFetcher.fetchStockPrice(stock, begin, end, Period.month);
        Assert.assertEquals(stockPriceList_month.size(),3);
    }

}
