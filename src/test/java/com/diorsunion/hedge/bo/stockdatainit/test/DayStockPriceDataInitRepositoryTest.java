package com.diorsunion.hedge.bo.stockdatainit.test;

import com.diorsunion.hedge.base.RealDataSourceBOBaseTest;
import com.diorsunion.hedge.bo.datasync.StockDataSync;
import com.diorsunion.hedge.dal.repository.stock.StockRepository;
import com.diorsunion.hedge.task.StockPriceSyncTask;
import com.diorsunion.hedge.util.ReflectionUtil;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;

/**
 * 初始化股票数据
 *
 * @author harley-dog on 2015/6/4.
 */
public class DayStockPriceDataInitRepositoryTest extends RealDataSourceBOBaseTest {

    static StockPriceSyncTask stockPriceSyncTask = new StockPriceSyncTask();
    @Resource @Qualifier("DayDataSync")
    StockDataSync dayDataSync;
    @Resource @Qualifier("WeekDataSync")
    StockDataSync weekDataSync;
    @Resource @Qualifier("MonthDataSync")
    StockDataSync monthDataSync;
    @Resource
    StockRepository stockRepository;

    @Before
    public void beforeClass() throws IllegalAccessException {
        ReflectionUtil.setValue(stockPriceSyncTask, "stockRepository",stockRepository);
    }

    @Test
    @Ignore
    public void testDay() {
        stockPriceSyncTask.run(dayDataSync, "UVXY");
    }

    @Test
//    @Ignore
    public void testWeek() {
        stockPriceSyncTask.run(weekDataSync,"UVXY");
    }

    @Test
    @Ignore
    public void testMonth() {
        stockPriceSyncTask.run(monthDataSync,"UVXY");
    }
}
