<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.diorsunion.hedge.dal.repository.stock.StockPriceRepository">
    <cache type="org.mybatis.caches.ehcache.LoggingEhcache"/>

    <resultMap id="StockPriceMap" type="StockPrice">
        <id column="id" property="id"/>
        <result column="thedate" property="thedate"/>
        <result column="pre_date" property="preDate"/>
        <result column="next_date" property="nextDate"/>
        <result column="open" property="open"/>
        <result column="close" property="close"/>
        <result column="low" property="low"/>
        <result column="high" property="high"/>
        <result column="percent" property="percent"/>
        <result column="chg" property="chg"/>
        <result column="dea" property="dea"/>
        <result column="dif" property="dif"/>
        <result column="macd" property="macd"/>
        <result column="ma5" property="ma5"/>
        <result column="ma10" property="ma10"/>
        <result column="ma20" property="ma20"/>
        <result column="ma30" property="ma30"/>
        <result column="turnrate" property="turnrate"/>
        <result column="volume" property="volume"/>
        <result column="split" property="split"/>
        <association property="stock" javaType="Stock">
            <id column="stock_code" property="code"/>
        </association>
        <association property="ex" javaType="StockPriceEx">
            <id column="ex_id" property="id"/>
            <id column="ex_thedate" property="thedate"/>
            <id column="ex_ema12" property="ema12"/>
            <id column="ex_ema26" property="ema26"/>
            <id column="ex_dif" property="dif"/>
            <id column="ex_dea" property="dea"/>
            <id column="ex_macd" property="macd"/>
            <id column="ex_macd_reverse_price" property="macdReversePrice"/>
            <id column="ex_macd_trend" property="macdTrend" typeHandler="com.diorsunion.hedge.common.StringEnumStatusHandler"/>
            <id column="ex_k" property="k"/>
            <id column="ex_d" property="d"/>
            <id column="ex_j" property="j"/>
            <id column="ex_rsi1" property="rsi1"/>
            <id column="ex_rsi2" property="rsi2"/>
            <id column="ex_rsi3" property="rsi3"/>
            <id column="ex_boll_up" property="bollUp"/>
            <id column="ex_boll_mid" property="bollMid"/>
            <id column="ex_boll_low" property="bollLow"/>
        </association>
    </resultMap>

    <sql id="price_ex_column">
            a.id,
            a.stock_code,
            a.thedate,
            a.pre_date,
            a.next_date,
            a.open,
            a.close,
            a.low,
            a.high,
            a.percent,
            a.chg,
            a.dea,
            a.dif,
            a.macd,
            a.ma5,
            a.ma10,
            a.ma20,
            a.ma30,
            a.turnrate,
            a.volume,
            a.split,
            b.id ex_id,
            b.thedate ex_thedate,
            b.ema12 ex_ema12,
            b.ema26 ex_ema26,
            b.dif ex_dif,
            b.dea ex_dea,
            b.macd ex_macd,
            b.macd_trend ex_macd_trend,
            b.macd_reverse_price ex_macd_reverse_price,
            b.k ex_k,
            b.d ex_d,
            b.j ex_j,
            b.rsi1 ex_rsi1,
            b.rsi2 ex_rsi2,
            b.rsi3 ex_rsi3,
            b.boll_up ex_boll_up,
            b.boll_mid ex_boll_mid,
            b.boll_low ex_boll_low
    </sql>

    <select id="get" resultMap="StockPriceMap" useCache="true">
        SELECT * FROM stock_price_${period}
        WHERE id = #{id}
    </select>

    <select id="findByIds" resultMap="StockPriceMap" useCache="true">
        SELECT * FROM stock_price_${period}
        <where>
            <foreach collection="ids" item="id" open=" id in (" close=")" separator=",">
                #{id}
            </foreach>
        </where>
    </select>

    <select id="find" resultMap="StockPriceMap" useCache="true">
        SELECT * FROM stock_price_${period}
        <where>
            <if test="query!=null and query.stock!=null">
                AND stock_code = #{query.stock.code}
            </if>
            <if test="query.thedate!=null">
                AND thedate = #{query.thedate}
            </if>
        </where>
    </select>

    <select id="findWithEx" resultMap="StockPriceMap" useCache="true">
        select
        <include refid="price_ex_column"/>
        from stock_price_${period} a
        inner join stock_price_${period}_ex b
        on a.id = b.id and a.stock_code = b.stock_code and a.thedate = b.thedate
        <where>
            <if test="query!=null and query.stock!=null">
                AND a.stock_code = #{query.stock.code}
            </if>
            <if test="query.thedate!=null">
                AND a.thedate = #{query.thedate}
            </if>
        </where>
    </select>

    <select id="getMaxDate" resultType="java.util.Date">
        <![CDATA[
         select max(thedate) from stock_price_${period} where stock_code = #{stock.code} and thedate <= #{thedate}
        ]]>
    </select>

    <select id="getMinDate" resultType="java.util.Date">
        <![CDATA[
         select min(thedate) from stock_price_${period} where stock_code = #{stock.code}
        ]]>
    </select>

    <select id="findStockPrice" resultMap="StockPriceMap">
        select
        id,
        stock_code,
        thedate,
        pre_date,
        next_date,
        open,
        close,
        low,
        high,
        percent,
        chg,
        dea,
        dif,
        macd,
        ma5,
        ma10,
        ma20,
        ma30,
        turnrate,
        volume,
        split
        from stock_price_${period}
        <where>
            stock_code = #{stock.code}
            <if test="begin!=null">
                and thedate <![CDATA[ >= ]]> #{begin}
            </if>
            <if test="end!=null">
                and thedate <![CDATA[ <= ]]> #{end}
            </if>
        </where>
        order by thedate asc
    </select>

    <select id="findStockPriceWithEx" resultMap="StockPriceMap">
        select
        <include refid="price_ex_column"/>
        from stock_price_${period} a
        left join stock_price_${period}_ex b
        on a.id = b.id and a.stock_code = b.stock_code and a.thedate = b.thedate
        <where>
            a.stock_code = #{stock.code}
            <if test="begin!=null">
                and a.thedate <![CDATA[ >= ]]> #{begin}
            </if>
            <if test="end!=null">
                and a.thedate <![CDATA[ <= ]]> #{end}
            </if>
        </where>
        order by a.thedate asc
    </select>

    <select id="findStockPriceWithExNum" resultMap="StockPriceMap">
        select
        <include refid="price_ex_column"/>
        from stock_price_${period} a
        inner join stock_price_${period}_ex b on a.id = b.id and a.stock_code = #{stock.code}
        inner join (
                    select id,thedate,stock_code  from stock_price_${period} c
                    where c.stock_code = #{stock.code} and c.thedate <![CDATA[ <= ]]> #{end}
                    order by c.thedate desc limit #{num}
        ) c on a.id = c.id
        order by a.thedate asc
    </select>

    <insert id="insert" useGeneratedKeys="true" keyProperty="entity.id">
        insert into stock_price_${period}
        (
        stock_code,
        thedate,
        pre_date,
        next_date,
        open,
        close,
        low,
        high,
        percent,
        chg,
        dea,
        dif,
        macd,
        ma5,
        ma10,
        ma20,
        ma30,
        turnrate,
        volume)
        values(
        #{entity.stock.code},
        #{entity.thedate},
        #{entity.preDate},
        #{entity.nextDate},
        #{entity.open},
        #{entity.close},
        #{entity.low},
        #{entity.high},
        #{entity.percent},
        #{entity.chg},
        #{entity.dea},
        #{entity.dif},
        #{entity.macd},
        #{entity.ma5},
        #{entity.ma10},
        #{entity.ma20},
        #{entity.ma30},
        #{entity.turnrate},
        #{entity.volume})
    </insert>

    <delete id="delete">
        delete from stock_price_${period} where id=#{id}
    </delete>

    <update id="update">
        update stock_price_${period}
        <set>
            <if test="stockPrice.nextDate!=null">
                next_date= #{stockPrice.nextDate},
            </if>
            <if test="stockPrice.preDate!=null">
                pre_date= #{stockPrice.preDate},
            </if>
        </set>
        <where>
            id = #{stockPrice.id}
        </where>
    </update>
</mapper>