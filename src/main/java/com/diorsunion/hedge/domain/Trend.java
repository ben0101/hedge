package com.diorsunion.hedge.domain;

import com.diorsunion.hedge.common.BaseShortEnum;
import com.diorsunion.hedge.common.BaseStringEnum;

/**
 * Created by wanglaoshi on 2016/4/17.
 */
public enum  Trend implements BaseStringEnum<Trend> {
    U("上升"),
    D("下降"),
    E("持平");

    public String desc;
    Trend(String desc){
        this.desc = desc;
    }
    public String getValue(){
        return toString();
    }
    public Trend getDefault(){
        return E;
    }

    public static final Trend judge(double ex,double cu){
        return ex<cu?U:(ex>cu)?D:E;
    }
}
