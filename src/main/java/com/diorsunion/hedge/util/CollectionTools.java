package com.diorsunion.hedge.util;

import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by harley-dog on 2016/5/5.
 */
public class CollectionTools {

    /**
     * @param list
     * @return
     */
    public final static double[] toDoubleArray(List<Double> list){
        if(CollectionUtils.isEmpty(list)){
            return new double[0];
        }
        Double[] a = new Double[list.size()];
        list.toArray(a);
        double[] r = new double[a.length];
        for(int i=0;i<a.length;i++){
            r[i] = a[i].doubleValue();
        }
        return r;
    }
}
