package com.diorsunion.hedge.util;

/**
 * Created by harley-dog on 2016/5/3.
 */
public final class MathUtils {

    /**
     * 约等于判断函数,废弃，可以用Assert.assertEquals(a,b,detal)来替换
     * @param f 小数点后四舍五入几位
     */
    @Deprecated
    public final static boolean equivalent(double a,double b,int f){
        double z = Math.pow(10,f);
        double za = z*a;
        double zb = z*b;
        long lza = Math.round(za);
        long lzb = Math.round(zb);
        return lza==lzb;
    }

    /**
     * 求最大最小值函数
     * @param d 数组
     * @return [0]:最大值 [1]:最小值
     */
    public final static double[] getMaxMin(double...d){
        double[] result = {Double.MIN_VALUE,Double.MAX_VALUE};
        for(int i=0;i<d.length;i++){
            if(d[i]>result[0]){
                result[0] = d[i];
            }
            if(d[i]<result[1]){
                result[1] = d[i];
            }
        }
        return result;
    }

    /**
     * 求和函数
     * @param a
     * @return
     */
    public final static double sum(double[] a){
        if(a==null){
            return 0;
        }
        double sum = 0;
        for(int i=0;i<a.length;i++){
            sum += a[i];
        }
        return sum;
    }

    /**
     * 求平均函数
     * @param a
     * @return
     */
    public final static double avg(double[] a){
        if(a==null){
            return 0;
        }
        double sum = 0;
        for(int i=0;i<a.length;i++){
            sum += a[i];
        }
        return sum/a.length;
    }

    /**
     * 求标准差函数,标准差公式
     * ⒈方差s^2=[(x1-x)^2+(x2-x)^2+......(xn-x)^2]/(n) （x为平均数）
     * ⒉标准差=方差的算术平方根
     * @return
     */
    public final static double std(double[] a){
        if(a==null){
            return 0;
        }
        double avg = avg(a);
        int num = a.length;
        double ma = 0;
        for(int i=0;i<num;i++){
            ma+=Math.pow(a[i] - avg, 2);
        }
        double sd =Math.sqrt(ma/num);
        return sd;
    }



}
