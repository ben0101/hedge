package com.diorsunion.hedge.web.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by harley-dog on 2016/4/28.
 */
@Controller
@Scope("prototype")
@RequestMapping("/test")
public class TestController {
    @Value("#{config['runtime.environment']}")
    private String runtime;

    @RequestMapping(value = "/runtime", method = {RequestMethod.GET})
    @ResponseBody
    public JsonResult<String> query() {
        return JsonResult.succResult("runtime",runtime);
    }
}
