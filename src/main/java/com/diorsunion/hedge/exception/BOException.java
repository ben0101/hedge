package com.diorsunion.hedge.exception;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class BOException extends RuntimeException {
    public BOException() {
    }

    public BOException(String message) {
        super(message);
    }

    public BOException(String message, Throwable cause) {
        super(message, cause);
    }

    public BOException(Throwable cause) {
        super(cause);
    }
}
