package com.diorsunion.hedge.common;

/**
 * Created by harley-dog on 2016/4/28.
 */
public interface BaseShortEnum {
    short getValue();
}
