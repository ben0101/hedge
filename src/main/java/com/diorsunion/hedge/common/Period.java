package com.diorsunion.hedge.common;

/**
 * Created by harley-dog<custer7572@163.com> on 15/12/15.
 */
public enum Period {
    day, week, month
}
