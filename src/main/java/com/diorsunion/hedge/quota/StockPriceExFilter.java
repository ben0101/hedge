package com.diorsunion.hedge.quota;

import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;

import java.util.List;

/**
 * 辅助指数计算器
 * Created by wanglaoshi on 2016/4/17.
 */
public interface StockPriceExFilter {
    /**
     * 计算各种量化指标,将量化指标计算后的值填充到stockPrice.ex变量中
     * @param stock 计算的股票
     * @param lastList 上一个作为基准的stockPrice集合，大部分的量化指标都需要借助上一个指标来进行计算,该参数可能为null
     * @param stockPriceList 需要计算的stockPrice集合
     */
    void filter(Stock stock,List<StockPrice> lastList,List<StockPrice> stockPriceList);
}
