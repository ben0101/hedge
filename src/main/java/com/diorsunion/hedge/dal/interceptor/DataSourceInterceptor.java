package com.diorsunion.hedge.dal.interceptor;

import com.diorsunion.hedge.dal.common.DataSourceType;
import com.diorsunion.hedge.dal.datasource.DatabaseContextHolder;
import org.aspectj.lang.JoinPoint;

/**
 * 数据源拦截器
 * Created by harley-dog  on 2016/5/5.
 */

//@Component
//@Aspect
public class DataSourceInterceptor {
    public void setdataSourceAccount(JoinPoint jp) {
        DatabaseContextHolder.setCustomerType(DataSourceType.ACCOUNT);
    }

    public void setdataSourceStock(JoinPoint jp) {
        DatabaseContextHolder.setCustomerType(DataSourceType.STOCK);
    }

//    @Around("execution(* com.diorsunion.hedge.bo.db.local.*.*(..))")
//    public void setdataSourceLocal(JoinPoint jp) {
//        DatabaseContextHolder.setCustomerType(DatabaseContextHolder.DataBaseType.LOCAL);
//    }
}
