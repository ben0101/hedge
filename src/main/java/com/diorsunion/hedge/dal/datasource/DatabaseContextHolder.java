package com.diorsunion.hedge.dal.datasource;

import com.diorsunion.hedge.dal.common.DataSourceType;

/**
 *
 * Created by harley-dog on 2016/5/5.
 */
public class DatabaseContextHolder {
    private static final ThreadLocal<DataSourceType> contextHolder = new ThreadLocal<DataSourceType>();

    public static void setCustomerType(DataSourceType customerType) {
        contextHolder.set(customerType);
    }

    public static DataSourceType getCustomerType() {
        return contextHolder.get();
    }

    public static void clearCustomerType() {
        contextHolder.remove();
    }
}
