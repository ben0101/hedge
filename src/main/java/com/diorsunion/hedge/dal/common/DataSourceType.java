package com.diorsunion.hedge.dal.common;

/**
 * Created by dingshan.yyj on 2016/5/5.
 */
public enum DataSourceType {
    ACCOUNT,
    STOCK,
    LOCAL;
}
