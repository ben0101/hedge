package com.diorsunion.hedge.dal.entity;

import com.diorsunion.hedge.exception.BOException;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public enum TradeType {
    BUY("买"),
    SELL("卖");
    public String desc;
    TradeType(String desc){
        this.desc = desc;
    }

    public static TradeType judgeByNum(int num){
        if(num==0)
            throw new BOException("交易数量不能为0");
        return num>0?BUY:SELL;
    }
}
