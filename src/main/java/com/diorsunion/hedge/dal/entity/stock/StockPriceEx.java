package com.diorsunion.hedge.dal.entity.stock;

import com.diorsunion.hedge.domain.Trend;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import java.util.Date;

/**
 * 一些扩展字段
 * Created by wanglaoshi on 2016/4/17.
 */
public class StockPriceEx {
    @GeneratedValue
    public int id;
    @JoinColumns(value = {
            @JoinColumn(name = "code")
    })
    public Stock stock;
    public Date thedate;
    public double ema12;
    public double ema26;
    public double dif;
    public double dea;
    public double macd;
    @Column(name = "macd_trend")//枚举类型的字段必须要加@Column
    public Trend macdTrend;//macd趋势
    public double macdReversePrice;//第二天的MACD值反转的临界点价格
    public double k;
    public double d;
    public double j;
    public double rsi1;
    public double rsi2;
    public double rsi3;
    public double bollUp;
    public double bollMid;
    public double bollLow;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StockPriceEx that = (StockPriceEx) o;

        if (id != that.id) return false;
        if (!stock.equals(that.stock)) return false;
        return thedate.equals(that.thedate);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + stock.hashCode();
        result = 31 * result + thedate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "StockPriceEx{" +
                "id=" + id +
                ", stock=" + stock +
                ", thedate=" + thedate +
                ", ema12=" + ema12 +
                ", ema26=" + ema26 +
                ", dif=" + dif +
                ", dea=" + dea +
                ", macd=" + macd +
                ", macdTrend=" + macdTrend +
                ", macdReversePrice=" + macdReversePrice +
                ", k=" + k +
                ", d=" + d +
                ", j=" + j +
                ", rsi1=" + rsi1 +
                ", rsi2=" + rsi2 +
                ", rsi3=" + rsi3 +
                ", bollUp=" + bollUp +
                ", bollMid=" + bollMid +
                ", bollLow=" + bollLow +
                '}';
    }
}
