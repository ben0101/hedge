package com.diorsunion.hedge.dal.entity.stock;

import com.diorsunion.hedge.domain.MACD;
import com.google.common.collect.Sets;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * @author harley-dog on 2015/6/4.
 */
@Entity
public class StockPrice implements Serializable {
    @GeneratedValue
    public int id;
    @JoinColumns(value = {
            @JoinColumn(name = "code")
    })
    public Stock stock;
    public Date thedate;
    public Date preDate;
    public Date nextDate;
    public double open;
    public double close;
    public double low;
    public double high;
    public double percent;
    public double chg;
    public double dea;
    public double dif;
    public double macd;
    public double ma5;
    public double ma10;
    public double ma20;
    public double ma30;
    public double turnrate;
    public long volume;
    public int split;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StockPrice that = (StockPrice) o;

        if (id != that.id) return false;
        if (!stock.equals(that.stock)) return false;
        return thedate.equals(that.thedate);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + stock.hashCode();
        result = 31 * result + thedate.hashCode();
        return result;
    }

    //根据价格类型返回相应的价格
    public double getPriceByType(PriceType priceType) {
        switch (priceType) {
            case OPEN:
                return open;
            case CLOSE:
                return close;
            case HIGH:
                return high;
            case LOW:
                return low;
            default:
                return high;
        }
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return "[" +
                (thedate==null?"no date":format.format(thedate)) +
                ", 开盘=" + new BigDecimal(open).setScale(2, BigDecimal.ROUND_HALF_UP) +
                ", 收盘=" + new BigDecimal(close).setScale(2, BigDecimal.ROUND_HALF_UP) +
                ", 最低=" + new BigDecimal(low).setScale(2, BigDecimal.ROUND_HALF_UP) +
                ", 最高=" + new BigDecimal(high).setScale(2, BigDecimal.ROUND_HALF_UP) +
                ", mcad=" + new BigDecimal(macd).setScale(2, BigDecimal.ROUND_HALF_UP) +
                ", 均5=" + new BigDecimal(ma5).setScale(2, BigDecimal.ROUND_HALF_UP) +
                ']';
    }

    public enum PriceType {
        OPEN("开盘价"),
        CLOSE("收盘价"),
        HIGH("最高价"),
        LOW("最低价");
        public final String name;

        PriceType(String name) {
            this.name = name;
        }

        public static PriceType getDefault() {
            return PriceType.CLOSE;
        }
    }



    //一些衍生字段
    public Set<MACD> macds = Sets.newHashSet();
    public StockPriceEx ex = new StockPriceEx();
}
