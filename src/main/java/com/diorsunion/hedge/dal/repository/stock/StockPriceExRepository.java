package com.diorsunion.hedge.dal.repository.stock;

import com.diorsunion.hedge.common.BaseRepository;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPriceEx;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by harley-dog on 2016/4/26.
 */
@Resource
public interface StockPriceExRepository extends BaseRepository<StockPriceEx, Integer> {
    Date getMaxDate(@Param("stock") Stock stock, @Param("thedate") Date thedate, @Param("period") Period period);
    StockPriceEx get(@Param("id") int id, @Param("period") Period period);
    void insert(@Param("entity") StockPriceEx entity, @Param("period") Period period);
    List<StockPriceEx> find(@Param("query") StockPriceEx query, @Param("period") Period period);
    List<StockPriceEx> findStockPriceEx(@Param("stock") Stock stock, @Param("begin") Date begin, @Param("end") Date end, @Param("period") Period period);
    void delete(@Param("id") int id, @Param("period") Period period);
}
