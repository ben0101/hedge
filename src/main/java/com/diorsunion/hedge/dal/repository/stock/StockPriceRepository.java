package com.diorsunion.hedge.dal.repository.stock;

import com.diorsunion.hedge.common.BaseRepository;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.stock.Stock;
import com.diorsunion.hedge.dal.entity.stock.StockPrice;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author harley-dog on 2015/6/4.
 */
@Resource
public interface StockPriceRepository extends BaseRepository<StockPrice, Integer> {
    void insert(@Param("entity") StockPrice entity, @Param("period") Period period);
    void delete(@Param("id") int id, @Param("period") Period period);
    void update(@Param("stockPrice") StockPrice stockPrice, @Param("period") Period period);
    StockPrice get(@Param("id") int id, @Param("period") Period period);
    List<StockPrice> findStockPrice(@Param("stock") Stock stock, @Param("begin") Date begin, @Param("end") Date end, @Param("period") Period period);
    List<StockPrice> findByIds(@Param("ids")List<Integer> ids,@Param("period") Period period);
    List<StockPrice> findStockPriceWithEx(@Param("stock") Stock stock, @Param("begin") Date begin, @Param("end") Date end, @Param("period") Period period);
    List<StockPrice> findStockPriceWithExNum(@Param("stock") Stock stock,@Param("end") Date end, @Param("num") int num, @Param("period") Period period);
    List<StockPrice> find(@Param("query") StockPrice query, @Param("period") Period period);
    List<StockPrice> findWithEx(@Param("query") StockPrice query, @Param("period") Period period);
    Date getMaxDate(@Param("stock") Stock stock, @Param("thedate") Date thedate, @Param("period") Period period);
    Date getMinDate(@Param("stock") Stock stock, @Param("period") Period period);
}
