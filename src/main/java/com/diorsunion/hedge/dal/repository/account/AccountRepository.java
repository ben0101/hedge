package com.diorsunion.hedge.dal.repository.account;

import com.diorsunion.hedge.common.BaseRepository;
import com.diorsunion.hedge.dal.entity.account.Account;

import javax.annotation.Resource;

/**
 * Created by harley-dog on 2016/4/26.
 */
@Resource
public interface AccountRepository extends BaseRepository<Account, Integer> {
}
